+++
title = "Proyecto docente"
weight = 30
pre = "<i class='fa fa-book'></i> "
+++

{{< button href="../docs/project.pdf" align="center" >}} Documento oficial {{< /button >}}

### ¿Qué vamos a estudiar?

La asigantura se centrará en el estudio de los conceptos de anillo, módulo y cuerpo y sus aplicaciones a las ecuaciones diofánticas, los operadores lineales, las construcciones geométricas con regla y compás y las ecuaciones polinómicas de grado superior en una variable. Seguiremos los capítulos correspondientes del libro titulado [*Algebra*](http://encore.fama.us.es/iii/encore/record/C__Rb1729980), de Michael Artin, basado en las notas que este profesor del Massachussets Institute of Technology usaba en sus clases. 

![book](../images/book.jpeg)

Concretamente usaremos los siguientes capítulos y secciones, referidos siempre a la segunda edición del libro:

* Capítulo 11, secciones de la 5, 7 y 8.

* Capítulo 12, secciones 2, 3 y 5.

* Capítulo 14, todas las secciones excepto 3, 6 y 9.

* Capítulo 15, todas las secciones excepto 6, 7, 9 y 10.

* Capítulo 16, todas las secciones excepto la 8 y la 9.

En el último de estos capítulos y en parte del penúltimo trabajaremos dentro de $\mathbb{C}$ para simplificar la exposición. En el último además veremos parte de la sección 5 del capítulo 7.

Recomendamos repasar conceptos vistos en Álgebra Básica que se corresponden con los siguientes capítulos y secciones del libro:

* Capítulo 1, sección 5.

* Capítulo 2.

* Capítulo 7, sección 1.

* Capítulo 11, secciones de la 1, 2, 3, 4 y 6.

* Capítulo 12, secciones 1, 3 y 4.

### ¿Cómo vamos a evaluar?

La evaluación continua se basará en dos exámenes. Estos exámenes durarán dos horas y se realizarán en horas de clase. El primero tendrá lugar en la última semana de noviembre y el segundo en las dos últimas horas de clase de la asignatura.

| Grupos            | Fecha del primer examen        | Fecha del segundo examen        |
| ------------------| ------------------------------ | ------------------------------- |
| A y B             | Jueves 21 de noviembre         | Jueves 16 de enero              |
| C                 | Miércoles 20 de noviembre      | Miércoles 15 de enero           |

Los contenidos de cada examen se fijarán con la suficiente antelación. La nota final de la evaluación continua será la media aritmética de ambos exámenes. Los estudiantes que no aprueben por este método podrán presentarse a las convocatorias oficiales en las fechas designadas por la Facultad de Matemáticas bajo las condiciones establecidas por la Universidad de Sevilla. 

Quien apruebe solo uno de los dos exámenes de la evaluación continua y tenga una media suspensa podrá, si así lo desea, presentarse al examen de la primera convocatoria y examinarse solo de la parte que suspendió. Su nota final será la media aritmética de la parte que aprobó y de la que se examine en la primera convocatoria. También podrán presentarse a la primera convocatoria aquellos estudiantes aprobados que deseen subir su nota, en ningún caso la bajarán.


### Clases

Cuatro horas semanales, de las cuales una hora será de problemas, de media a lo largo del semestre.

### Tutorías

| Profesores                      | Días y horas                                                 |
|---------------------------------|--------------------------------------------------------------|
| Fernando Muro (coordinador)     | Martes, miércoles y jueves de 9:30 a 11:30                   |
| Antonio Rojas                   | Lunes y martes de 11:00 a 13:00 y miércoles de 17:00 a 19:00 |
| Jesús Soto                      | Martes, Jueves y Viernes de 15:30 a 17:30                    |

